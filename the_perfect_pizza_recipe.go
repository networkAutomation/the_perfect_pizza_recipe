package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func PercentInt(pcent int, all int) float64 {
	percent := ((float64(all) * float64(pcent)) / float64(100))
	return percent
}

func PercentFloat(pcent float64, all int) float64 {
	percent := ((float64(all) * pcent) / float64(100))
	return percent
}

func main() {
	// flour input
	flourQ := bufio.NewReader(os.Stdin)
	fmt.Print("\nHow many grams of flour are you going to use? > ")
	flour, err := flourQ.ReadString('\n')
	if err != nil {
		log.Fatal(fmt.Println("ERROR - Cannot read from stdin"))
	}

	flourParsed := strings.Replace(flour, "\n", "", -1)
	flourInt, err := strconv.Atoi(flourParsed)
	if err != nil {
		log.Fatal(fmt.Println("ERROR - Value must be an integer"))
	}

	// room temperature input
	roomTempQ := bufio.NewReader(os.Stdin)
	fmt.Print("\nWhat is the actual room temperature? > ")
	roomTemp, err := roomTempQ.ReadString('\n')
	if err != nil {
		log.Fatal(fmt.Println("ERROR - Cannot read from stdin"))
	}

	roomTempParsed := strings.Replace(roomTemp, "\n", "", -1)
	roomTempInt, err := strconv.Atoi(roomTempParsed)
	if err != nil {
		log.Fatal(fmt.Println("ERROR - Value must be an integer"))
	}

loop:
	for {
		// pizza type input
		pizzaTypeQ := bufio.NewReader(os.Stdin)
		fmt.Print("\nDo you want make C(lassic) Pizza or B(iga) Pizza? >")
		pizzaType, err := pizzaTypeQ.ReadString('\n')
		if err != nil {
			log.Fatal(fmt.Println("ERROR - Cannot read from stdin"))
		}

		// switch expression
		switch pizzaType {
		case "C\n":
			pizza(flourInt, roomTempInt)
			break loop
		case "B\n":
			biga(flourInt, roomTempInt)
			break loop
		default:
			fmt.Println("\nPlease type \"C\" for Classic Piza or \"B\" for Biga Pizza")
		}
	}
}

func pizza(f int, rt int) {
	// temperature variables - Napels style
	ingredientsTemp := rt - 1
	constant := 9
	waterTemp := 75 - rt - ingredientsTemp - constant
	t := time.Now().Add(6 * time.Hour)
	timePizzaReady := t.Format(time.RFC822)

	// dough ingredients quantity
	dougFlour := f
	dougWater := PercentInt(66, dougFlour)
	dougYeast := PercentFloat(0.1, dougFlour)
	dougSalt := PercentInt(2, dougFlour)
	starterFlour := PercentInt(10, dougFlour)

	// procedure
	fmt.Printf(`
    ########################################################################
    ### THIS IS THE PERFECT RECIPE FOR MAKING A PERFECT PIZZA AT HOME   ###
    ########################################################################

    INTRO:
    All ingredients are expressed in GRAMS.
    For liquids like water, oil, malt you can safely use MILLILITERS.
    Use fresh yeast, not dry yeast.
    Flour must have the following requirement: 0,40 < P/L < 0,60; W > 300

    If you want a good result, you need to use good quality ingredients. This
    is the secret. If you buy first price stuff, probably your pizza would
    taste like the Pizza Hut/Papa Johns/Domino ones.

    N.B
    Water temperature must be %v

    PIZZA DOUGH INGREDIENTS:
    Flour: %v
    Salt: %v
    Water: %v
    Yeast: %v

    PREPARATION:
    For PIZZA DOUGH: melt the yeast into the water add roughly %v grams of flour and knead it.
    Add the salt and make sure it get completely absorbed then add flour and knead the ingredients for 5 minutes.

    Leave the dough rest for 4-6 hours at 15-16°C. Pizza dough will be ready %v
    Higher the temperature, lower will be the resting time (for each °C rising increse 8-12%%).
    `, waterTemp, dougFlour, dougSalt, dougWater, dougYeast, starterFlour, timePizzaReady)
}

func biga(f int, rt int) {
	// temperature variables
	ingredientsTemp := rt - 1
	waterTemp := 55 - (ingredientsTemp + rt)

	// biga ingredients quantity
	bigaFlour := int(PercentInt(20, f))
	bigaWater := PercentInt(44, bigaFlour)
	bigaYeast := PercentInt(1, bigaFlour)

	// dough ingredients quantity
	dougFlour := f - bigaFlour // equal to 80% for biga equal to 20%
	dougSalt := PercentInt(3, f)
	dougMalt := PercentInt(1, f)
	dougWater := PercentInt(56, f) - bigaWater
	dougOliveOil := PercentInt(3, f)
	dougYeast := PercentFloat(0.02, dougFlour)
	dougBiga := float64(bigaFlour) + float64(bigaWater) + float64(bigaYeast)
	starterFlour := PercentInt(10, dougFlour)

	// biga option variables
  var bigaType string
  var bigaRising int
  var bigaTemp string
  var tB time.Time
  var tP time.Time
  var timeBigaReady string
  var timePizzaReady string

	// biga option
  loop:
	for {
		bigaQ := bufio.NewReader(os.Stdin)
		fmt.Print("\nShort or Long Biga? S(hort) L(ong) N(ot sure) > ")
		biga, err := bigaQ.ReadString('\n')
		if err != nil {
			log.Fatal(fmt.Println("ERROR - Cannot read from stdin"))
		}
		// switch expression
		switch biga {
		case "S\n":
			bigaType = "Short"
			bigaRising = 16
			bigaTemp = "18-19°C"
			tB = time.Now().Add(16 * time.Hour)
			tP = time.Now().Add(22 * time.Hour)
			timeBigaReady = tB.Format(time.RFC822)
			timePizzaReady = tP.Format(time.RFC822)
			break loop
		case "L\n":
			bigaType = "Long"
			bigaRising = 48
			bigaTemp = "4°C for 24 hours then 18-19°C for the remaining 24 hours"
			tB = time.Now().Add(16 * time.Hour)
			tP = time.Now().Add(22 * time.Hour)
			timeBigaReady = tB.Format(time.RFC822)
			timePizzaReady = tP.Format(time.RFC822)
			break loop
		default:
			fmt.Println("\nPlease type \"C\" for Classic Piza or \"B\" for Biga Pizza")
		}
	}

	// procedure
	fmt.Printf(`
    ###########################################################################
    ### THIS IS THE PERFECT RECIPE FOR MAKING A PERFECT BIGA PIZZA AT HOME ###
    ###########################################################################

    INTRO:
    All ingredients are expressed in GRAMS.
    For liquids like water, oil, malt you can safely use MILLILITERS.
    Use fresh yeast, not dry yeast.
    Flour must have the following requirement: 0,40 < P/L < 0,60; W > 300

    If you want a good result, you need to use good quality ingredients. This
    is the secret. If you buy first price stuff, probably your pizza would
    taste like the Pizza Hut/Papa Johns/Domino ones.

    N.B
    Water temperature must be %v

    BIGA INGREDIENTS:
    Type: %v
    Flour: %v
    Water: %v
    Yeast: %v

    PIZZA DOUGH INGREDIENTS:
    Flour: %v
    Salt: %v
    Malt: %v
    Water: %v
    Olive oil: %v
    Yeast: %v
    Biga: %v

    PREPARATION:
    Let's make the BIGA first: melt the yeast into the water, then add flour and knead the ingredients for 5 minutes.
    Let the Biga rest for %v hours at %v. Biga will be ready %v

    For PIZZA DOUGH: melt the yeast into the water like we have done for the Biga, add roughly %v grams of flour and knead it.
    Add the salt and make sure it get completely absorbed. Once done it, add the remaining flour, oil and malt. As last, add %v
    grams of Biga and knead all the ingredients together.

    Leave the dough rest for 4-6 hours at 15-16°C. Pizza dough will be ready %v.
    Higher the temperature, lower will be the resting time (for each °C rising increse 8-12%).
    `, waterTemp, bigaType, bigaFlour, bigaWater, bigaYeast, dougFlour, dougSalt, dougMalt, dougWater, dougOliveOil,
		dougYeast, dougBiga, bigaRising, bigaTemp, timeBigaReady, starterFlour, dougBiga, timePizzaReady)
}
