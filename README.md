### CLASSIC PIZZA EXAMPE

```
How many grams of flour are you going to use? > 2000

What is the actual room temperature? > 22

Do you want make C(lassic) Pizza or B(iga) Pizza? >C

    ########################################################################
    ### THIS IS THE PERFECT RECIPE FOR MAKING A PERFECT PIZZA AT HOME   ###
    ########################################################################

    INTRO:
    All ingredients are expressed in GRAMS.
    For liquids like water, oil, malt you can safely use MILLILITERS.
    Use fresh yeast, not dry yeast.
    Flour must have the following requirement: 0,40 < P/L < 0,60; W > 300

    If you want a good result, you need to use good quality ingredients. This
    is the secret. If you buy first price stuff, probably your pizza would
    taste like the Pizza Hut/Papa Johns/Domino ones.

    N.B
    Water temperature must be 23

    PIZZA DOUGH INGREDIENTS:
    Flour: 2000.0
    Salt: 40.0
    Water: 1320.0
    Yeast: 2.0

    PREPARATION:
    For PIZZA DOUGH: melt the yeast into the water add roughly 200.0 grams of flour and knead it.
    Add the salt and make sure it get completely absorbed then add flour and knead the ingredients for 5 minutes.

    Leave the dough rest for 4-6 hours at 15-16°C. Pizza dough will be ready Wed Nov 28 20:07:54 2018 Higher the temperature, lower will be the resting time.
```

### BIGA PIZZA EXAMPLE

```
How many grams of flour are you going to use? > 10000

What is the actual room temperature? > 22

Do you want make C(lassic) Pizza or B(iga) Pizza? >B

Short or Long Biga? S(hort) L(long) N(ot sure) > L

    ###########################################################################
    ### THIS IS THE PERFECT RECIPE FOR MAKING A PERFECT BIGA PIZZA AT HOME ###
    ###########################################################################

    INTRO:
    All ingredients are expressed in GRAMS.
    For liquids like water, oil, malt you can safely use MILLILITERS.
    Use fresh yeast, not dry yeast.
    Flour must have the following requirement: 0,40 < P/L < 0,60; W > 300

    If you want a good result, you need to use good quality ingredients. This
    is the secret. If you buy first price stuff, probably your pizza would
    taste like the Pizza Hut/Papa Johns/Domino ones.

    N.B
    Water temperature must be 13

    BIGA INGREDIENTS:
    Type: long
    Flour: 2000.0
    Water: 880.0
    Yeast: 20.0

    PIZZA DOUGH INGREDIENTS:
    Flour: 8000.0
    Salt: 300.0
    Malt: 100.0
    Water: 4720.0
    Olive oil: 300.0
    Yeast: 1.6
    Biga: 2900.0

    PREPARATION:
    Let's make the BIGA first: melt the yeast into the water, then add flour and knead the ingredients for 5 minutes.
    Let the Biga rest for 48 hours at 4°C for 24 hours then 18-19°C for the remaining 24 hours. Biga will be ready Fri Nov 30 14:07:08 2018

    For PIZZA DOUGH: melt the yeast into the water like we have done for the Biga, add roughly 1000.0 grams of flour and knead it.
    Add the salt and make sure it get completely absorbed. Once done it, add the remaining flour, oil and malt. As last, add 2900.0
    grams of Biga and knead all the ingredients together.

    Leave the dough rest for 4-6 hours at 15-16°C. Pizza dough will be ready Fri Nov 30 20:07:08 2018. Higher the temperature, lower will be the resting time.
    Cook in the oven at 350°C - static.
```

### PIZZA IN PALA

![Data Source](screenshots/pala.png)

### BIGA

![Data Source](screenshots/biga.png)

### DOUGH

![Data Source](screenshots/dough.png)
