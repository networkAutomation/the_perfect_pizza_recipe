#/usr/bin/env  python3
import datetime
import sys

def main():
    # user type
    while True:
        # pizza type
        pizza_type = input("\nVuoi fare la C(lassica) Pizza Napoletana o Pizza con B(iga) ? >")
        if pizza_type == "C":
            pizza()
            break
        elif pizza_type == "B":
            biga()
            break
        else:
            print("\nDigita \"C\" per Classica) Pizza Napoletana o \"B\" per la Pizza con Biga ")

def pizza():
    # temperature variables - Napels style
    ingredients_temperature = room_temperature - 1
    constant = 9
    water_temperature = 75 - room_temperature - ingredients_temperature - constant
    time_pizza_ready = datetime.datetime.now() + datetime.timedelta(hours=6)

    # dough ingredients quantity
    doug_g_flour = flour
    doug_g_water = flour * (66/100)
    doug_g_yeast = doug_g_flour * (0.1/100)
    doug_g_salt = flour * (2/100)

    # procedure
    print("""
    ###########################################################################
    ### QUESTA E' LA RICETTA PERFETTA PER UNA PIZZA PERFETTA FATTA IN CASA  ###
    ###########################################################################

    INTRO:
    Tutti gli ingredienti sono indicati in GRAMMI.
    Per i liquidi come acqua, olio e malto possono essere usati i MILLILITRI come riferimento.
    Usa il lievito fresco, non quello secco.
    La farina deve avere le seguenti caratteristiche: 0,40 < P/L < 0,60; W > 300

    Se vuoi ottenere un buon risultato, devi utilizzare ingredienti di prima qualita'
    Questo e' il segreto. Se usi ingredienti economici, molto probabilmente la tua pizza
    avra' la stessa consistenza e lo stesso sapore di quella surgelata del supermercato.

    N.B
    La temperatura dell' acqua deve essere di {water_temperature}

    IMPASTO PIZZA INGREDIENTI:
    Farina: {doug_g_flour:.1f}
    Sale: {doug_g_salt:.1f}
    Acqua: {doug_g_water:.1f}
    Lievito: {doug_g_yeast:.1f}

    PREPARAZIONE:
    Per IMPASTO PIZZA: sciogliere il lievito nell' acqua ed aggiungere piu' o meno {starter_flour:.1f} grammi di farina e mescolare.
    Aggiungere il sale ed assicurarsi che venga completamente assorbito dopodiche' aggiungere la rimanente farina
    e mescolare gli ingredienti per almeno 5 minuti

    Lasciare l' impasto a riposare per 4-6 ore a 15-16°C. L' impasto sara' pronto {time_pizza_ready}.
    Piu' alta e' la temperatura ambiente e minore sara' il tempo di lievitazione (per ogni grado °C la lievitazione aumenta dell' 8-12%)
    """.format(water_temperature=water_temperature, doug_g_flour=doug_g_flour, doug_g_salt=doug_g_salt, doug_g_water=doug_g_water, doug_g_yeast=doug_g_yeast,
    starter_flour=flour * (10/100), time_pizza_ready=time_pizza_ready.strftime("%c")))

def biga():
    # biga type
    while True:
        biga = input("\nBiga Corta o Biga Lunga? C(orta) L(unga) N(on saprei) > ")
        if biga == "C":
            biga_rising = 16
            biga_temp = "18-19°C"
            time_biga_ready = datetime.datetime.now() + datetime.timedelta(hours=16)
            time_pizza_ready = datetime.datetime.now() + datetime.timedelta(hours=22)
            break
        elif biga == "L":
            biga_rising = 48
            biga_temp = "4°C per 24 ore poi 18-19°C per le rimanenti 24 ore"
            time_biga_ready = datetime.datetime.now() + datetime.timedelta(hours=48)
            time_pizza_ready = datetime.datetime.now() + datetime.timedelta(hours=54)
            break
        elif biga == "N":
            biga_rising = 16
            biga_temp = "18-19°C"
            time_biga_ready = datetime.datetime.now() + datetime.timedelta(hours=16)
            time_pizza_ready = datetime.datetime.now() + datetime.timedelta(hours=22)
            break
        else:
            print("\nDigita \"C\" per Corta, \"L\" per Lunga \"N\" se non sei sicuro quale opzione scegliere" )

    # temperature variables
    ingredients_temperature = room_temperature - 2
    water_temperature = 55 - (room_temperature + ingredients_temperature)

    # biga ingredients quantity
    biga_g_flour = flour * (20/100)
    biga_g_water = biga_g_flour * (44/100)
    biga_g_yeast = biga_g_flour * (1/100)

    # dough ingredients quantity
    doug_g_flour = flour - biga_g_flour # equal to 80% for biga equal to 20%
    doug_g_salt = flour * (3/100)
    doug_g_malt = flour * (1/100)
    doug_g_water = flour * (56/100) - biga_g_water
    doug_g_olive_oil = flour * (3/100)
    doug_g_yeast = doug_g_flour * (0.02/100)
    doug_g_biga = biga_g_flour + biga_g_water + biga_g_yeast

    # procedure
    print("""
    ###########################################################################
    ### QUESTA E' LA RICETTA PERFETTA PER UNA PIZZA PERFETTA FATTA IN CASA  ###
    ###########################################################################

    INTRO:
    Tutti gli ingredienti sono indicati in GRAMMI.
    Per i liquidi come acqua, olio e malto possono essere usati i MILLILITRI come riferimento.
    Usa il lievito fresco, non quello secco.
    La farina deve avere le seguenti caratteristiche: 0,40 < P/L < 0,60; W > 300

    Se vuoi ottenere un buon risultato, devi utilizzare ingredienti di prima qualita'
    Questo e' il segreto. Se usi ingredienti economici, molto probabilmente la tua pizza
    avra la stessa consistenza e lo stesso sapore di quella surgelata del supermercato.

    N.B
    La temperatura dell' acqua deve essere di {water_temperature}

    INGREDIENTI BIGA:
    Tipo: {biga_lenght}
    Farina: {biga_g_flour:.1f}
    Acqua: {biga_g_water:.1f}
    Lievito: {biga_g_yeast:.1f}

    INGREDIENTI IMPASTO PIZZA:
    Farina: {doug_g_flour:.1f}
    Sale: {doug_g_salt:.1f}
    Malto: {doug_g_malt:.1f}
    Acqua: {doug_g_water:.1f}
    Olio di Oliva: {doug_g_olive_oil:.1f}
    Lievito: {doug_g_yeast:.1f}
    Biga: {doug_g_biga:.1f}

    PREPARAZIONE:
    Prepariamo prima la BIGA: sciogliere il lievito nell' acqua, successivamente aggiugnere la farina e mescolare per 5 minuti.
    Lasciare la Biga riposare per {biga_rising} ore a {biga_temp}. La Biga sara' pronta {time_biga_ready}

    Per IMPASTO PIZZA: sciogliere il lievito nell' acqua ed aggiungere piu' o meno {starter_flour:.1f} grammi di farina e mescolare.
    Aggiungere il sale ed assicurarsi che venga completamente assorbito dopodiche' aggiungere la rimanente farina, olio e malto.
    Come ultimo passaggio, aggiungere {doug_g_biga:.1f} grammi di Biga e impastare per altri 5 minuti.

    Lasciare l' impasto a riposare per 4-6 ore a 15-16°C. L' impasto sara' pronto {time_pizza_ready}. Piu' alta e' la temperatura ambiente
    e minore sara il tempo di lievitazione (per ogni grado °C la lievitazione aumenta dell' 8-12%)
    Cuocere in forno statico a 350°C per 5 minuti massimo.
    """.format(biga_lenght="corta" if biga == "S" else "lunga", water_temperature=water_temperature, biga_g_flour=biga_g_flour, biga_g_water=biga_g_water,
    biga_g_yeast=biga_g_yeast, doug_g_flour=doug_g_flour, doug_g_salt=doug_g_salt, doug_g_malt=doug_g_malt, doug_g_water=doug_g_water, doug_g_olive_oil=doug_g_olive_oil,
    doug_g_yeast=doug_g_yeast, biga_rising=biga_rising, biga_temp=biga_temp, time_biga_ready=time_biga_ready.strftime("%c"),
    starter_flour=flour * (10/100),doug_g_biga=doug_g_biga, time_pizza_ready=time_pizza_ready.strftime("%c")))

if __name__ == '__main__':
    try:
        flour = int(input("\nQuanti grammi di farina usi? > "))
    except ValueError:
        print("\nERRORE - Farina deve essere espressa in cifre\n")
        sys.exit(1)

    try:
        room_temperature = int(input("\nQuale e' l'attuale temperatura ambiente? > "))
    except ValueError:
        print("\nERROR - Temperatura ambiente deve essere espressa in cifre\n")
        sys.exit(1)

    main()
