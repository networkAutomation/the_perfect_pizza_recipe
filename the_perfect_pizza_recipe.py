#/usr/bin/env  python3
import datetime
import sys

def main():
    # user type
    while True:
        # pizza type
        pizza_type = input("\nDo you want make C(lassic) Pizza or B(iga) Pizza? >")
        if pizza_type == "C":
            pizza()
            break
        elif pizza_type == "B":
            biga()
            break
        else:
            print("\nPlease type \"C\" for Classic Piza or \"B\" for Biga Pizza")

def pizza():
    # temperature variables - Napels style
    ingredients_temperature = room_temperature - 1
    constant = 9
    water_temperature = 75 - room_temperature - ingredients_temperature - constant
    time_pizza_ready = datetime.datetime.now() + datetime.timedelta(hours=6)

    # dough ingredients quantity
    doug_g_flour = flour
    doug_g_water = flour * (66/100)
    doug_g_yeast = doug_g_flour * (0.1/100)
    doug_g_salt = flour * (2/100)

    # procedure
    print("""
    ########################################################################
    ### THIS IS THE PERFECT RECIPE FOR MAKING A PERFECT PIZZA AT HOME   ###
    ########################################################################

    INTRO:
    All ingredients are expressed in GRAMS.
    For liquids like water, oil, malt you can safely use MILLILITERS.
    Use fresh yeast, not dry yeast.
    Flour must have the following requirement: 0,40 < P/L < 0,60; W > 300

    If you want a good result, you need to use good quality ingredients. This
    is the secret. If you buy first price stuff, probably your pizza would
    taste like the Pizza Hut/Papa Johns/Domino ones.

    N.B
    Water temperature must be {water_temperature}

    PIZZA DOUGH INGREDIENTS:
    Flour: {doug_g_flour:.1f}
    Salt: {doug_g_salt:.1f}
    Water: {doug_g_water:.1f}
    Yeast: {doug_g_yeast:.1f}

    PREPARATION:
    For PIZZA DOUGH: melt the yeast into the water add roughly {starter_flour:.1f} grams of flour and knead it.
    Add the salt and make sure it get completely absorbed then add flour and knead the ingredients for 5 minutes.

    Leave the dough rest for 4-6 hours at 15-16°C. Pizza dough will be ready {time_pizza_ready}.
    Higher the temperature, lower will be the resting time (for each °C rising increse 8-12%).
    """.format(water_temperature=water_temperature, doug_g_flour=doug_g_flour, doug_g_salt=doug_g_salt, doug_g_water=doug_g_water, doug_g_yeast=doug_g_yeast,
    starter_flour=flour * (10/100), time_pizza_ready=time_pizza_ready.strftime("%c")))

def biga():
    # biga type
    while True:
        biga = input("\nShort or Long Biga? S(hort) L(ong) N(ot sure) > ")
        if biga == "S":
            biga_rising = 16
            biga_temp = "18-19°C"
            time_biga_ready = datetime.datetime.now() + datetime.timedelta(hours=16)
            time_pizza_ready = datetime.datetime.now() + datetime.timedelta(hours=22)
            break
        elif biga == "L":
            biga_rising = 48
            biga_temp = "4°C for 24 hours then 18-19°C for the remaining 24 hours"
            time_biga_ready = datetime.datetime.now() + datetime.timedelta(hours=48)
            time_pizza_ready = datetime.datetime.now() + datetime.timedelta(hours=54)
            break
        elif biga == "N":
            biga_rising == 16
            break
        else:
            print("\nPlease type \"S\" for Short, \"L\" for Long \"N\" if not sure" )

    # temperature variables
    ingredients_temperature = room_temperature - 2
    water_temperature = 55 - (room_temperature + ingredients_temperature)

    # biga ingredients quantity
    biga_g_flour = flour * (20/100)
    biga_g_water = biga_g_flour * (44/100)
    biga_g_yeast = biga_g_flour * (1/100)

    # dough ingredients quantity
    doug_g_flour = flour - biga_g_flour # equal to 80% for biga equal to 20%
    doug_g_salt = flour * (3/100)
    doug_g_malt = flour * (1/100)
    doug_g_water = flour * (56/100) - biga_g_water
    doug_g_olive_oil = flour * (3/100)
    doug_g_yeast = doug_g_flour * (0.02/100)
    doug_g_biga = biga_g_flour + biga_g_water + biga_g_yeast

    # procedure
    print("""
    ###########################################################################
    ### THIS IS THE PERFECT RECIPE FOR MAKING A PERFECT BIGA PIZZA AT HOME ###
    ###########################################################################

    INTRO:
    All ingredients are expressed in GRAMS.
    For liquids like water, oil, malt you can safely use MILLILITERS.
    Use fresh yeast, not dry yeast.
    Flour must have the following requirement: 0,40 < P/L < 0,60; W > 300

    If you want a good result, you need to use good quality ingredients. This
    is the secret. If you buy first price stuff, probably your pizza would
    taste like the Pizza Hut/Papa Johns/Domino ones.

    N.B
    Water temperature must be {water_temperature}

    BIGA INGREDIENTS:
    Type: {biga_lenght}
    Flour: {biga_g_flour:.1f}
    Water: {biga_g_water:.1f}
    Yeast: {biga_g_yeast:.1f}

    PIZZA DOUGH INGREDIENTS:
    Flour: {doug_g_flour:.1f}
    Salt: {doug_g_salt:.1f}
    Malt: {doug_g_malt:.1f}
    Water: {doug_g_water:.1f}
    Olive oil: {doug_g_olive_oil:.1f}
    Yeast: {doug_g_yeast:.1f}
    Biga: {doug_g_biga:.1f}

    PREPARATION:
    Let's make the BIGA first: melt the yeast into the water, then add flour and knead the ingredients for 5 minutes.
    Let the Biga rest for {biga_rising} hours at {biga_temp}. Biga will be ready {time_biga_ready}

    For PIZZA DOUGH: melt the yeast into the water like we have done for the Biga, add roughly {starter_flour:.1f} grams of flour and knead it.
    Add the salt and make sure it get completely absorbed. Once done it, add the remaining flour, oil and malt. As last, add {doug_g_biga:.1f}
    grams of Biga and knead all the ingredients together.

    Leave the dough rest for 4-6 hours at 15-16°C. Pizza dough will be ready {time_pizza_ready}.
    Higher the temperature, lower will be the resting time (for each °C rising increse 8-12%).
    """.format(biga_lenght="short" if biga == "S" else "long", water_temperature=water_temperature, biga_g_flour=biga_g_flour, biga_g_water=biga_g_water,
    biga_g_yeast=biga_g_yeast, doug_g_flour=doug_g_flour, doug_g_salt=doug_g_salt, doug_g_malt=doug_g_malt, doug_g_water=doug_g_water, doug_g_olive_oil=doug_g_olive_oil,
    doug_g_yeast=doug_g_yeast, biga_rising=biga_rising, biga_temp=biga_temp, time_biga_ready=time_biga_ready.strftime("%c"),
    starter_flour=flour * (10/100),doug_g_biga=doug_g_biga, time_pizza_ready=time_pizza_ready.strftime("%c")))


if __name__ == '__main__':
    try:
        flour = int(input("\nHow many grams of flour are you going to use? > "))
    except ValueError:
        print("\nERROR - flour must be numerical values\n")
        sys.exit(1)

    try:
        room_temperature = int(input("\nWhat is the actual room temperature? > "))
    except ValueError:
        print("\nERROR - room temperature must be numerical values\n")
        sys.exit(1)

    main()
